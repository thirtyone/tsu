<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tsu' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Qv(gNcaX_`}~o9W2-_s> nsd )&u.wQXxF2zmCzTX6kdithGY=*N..eiUc//y0CN' );
define( 'SECURE_AUTH_KEY',  '>X;s-_UGTkxx/0j3IMc0UvImtPn4X)+`Sm29f;oZgG^q+9&E r|dDx+U(YWaLWhn' );
define( 'LOGGED_IN_KEY',    'J!DUj<8=2n6VSTro4AzhtfjIOP)Z1x3lHLk;{1x3pqhkeWhsw[$.haW*fv{i)Vk)' );
define( 'NONCE_KEY',        '1Fr6p|88?}yQVkl77&(;,p&7:0?61mc][swy=j=CYkY)_~fz+6@Yr]Zp/i7K3*6l' );
define( 'AUTH_SALT',        '7/jx_e<z>*6n 8 k2}X;LXifsbeg#yw?% wjw:Ir<XNZ2kkNeiYpSXs<PS05Zv#r' );
define( 'SECURE_AUTH_SALT', '>r=+9>._@L8%]<f>b6[IDMFg_$MZ0#hr^zOOMVk,*C{8T{7bqFV%N)bR niE4n#P' );
define( 'LOGGED_IN_SALT',   '/MG`N|=h3/ L`-d4eO$ 5(`i Mr?.C %3(da%%_qqaGV@VOL:y@{e4TW4I%H}@7,' );
define( 'NONCE_SALT',       '|?>+2<3]Z7qW/By;)8P0@:Q1@gMcP(x/ cZFN=On;]U6Sm=>)y|IcQizUn?[c[37' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
